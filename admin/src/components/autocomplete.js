import React, { useState } from 'react';
import PropTypes from 'prop-types';
import deburr from 'lodash/deburr';
import Downshift from 'downshift';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';

function renderInput(inputProps) {
  const { InputProps, classes, ref, ...other } = inputProps;

  return (
    <TextField
      InputProps={{
        inputRef: ref,
        classes: {
          root: classes.inputRoot,
          input: classes.inputInput,
        },
        ...InputProps,
      }}
      {...other}
    />
  );
}

function renderSuggestion({ suggestion, index, itemProps, highlightedIndex, selectedItem }) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || '').indexOf(suggestion.nombre) > -1;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.id}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
    >
      {suggestion.nombre} {suggestion.tipo} {suggestion.rodado} {suggestion.color}  {new Intl.NumberFormat("de-DE", {style: "currency", currency: "ARS"}).format(suggestion.precio)}
    </MenuItem>
  );
}
renderSuggestion.propTypes = {
  highlightedIndex: PropTypes.number,
  index: PropTypes.number,
  itemProps: PropTypes.object,
  selectedItem: PropTypes.string,
  suggestion: PropTypes.shape({ name: PropTypes.string }).isRequired,
};

function setFullStringSearch(suggestion) {
  return `${suggestion.nombre}`;
}

function getSuggestions(value, suggestions, isBlured) {
    const inputValue = deburr(value.trim()).toLowerCase();
    const inputLength = inputValue.length;
    
    let count = 0;

    if (isBlured) {
        if (inputLength === 0) {
            return suggestions
        } else {
            return suggestions.filter(suggestion => {
                const keep =
                  count < 1000  && setFullStringSearch(suggestion).toLowerCase().includes(inputValue);
        
                if (keep) {
                  count += 1;
                }
        
                return keep;
              });
        }
    } else {
        if (inputLength === 0) {
            return []
        }
    }

    return [];
}

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    flexGrow: 1,
    position: 'relative',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
    overflowY: 'auto',
    maxHeight: 300,
    zIndex: 9999
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  inputRoot: {
    flexWrap: 'wrap',
  },
  inputInput: {
    width: 'auto',
    flexGrow: 1,
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
});

let popperNode;

function IntegrationDownshift(props) {
  const { classes, suggestions, label, onSelect, onChange, defaultValue, style, onKeyPress, showAllSuggestions, key } = props;

  const [ isBlured, setIsBlured ] = useState(false);
  const [ isSelected, setIsSelected ] = useState(false);
  
  console.log('sugerencias', suggestions)

  const onSelectHandler = evt => {
    setIsSelected(true); 

    console.log('selecting');
  }

  return (
    <div className={classes.root}>
      <Downshift id="downshift-simple" onSelect={onSelectHandler} onChange={onChange} initialInputValue={defaultValue} >
        {({
          getInputProps,
          getItemProps,
          getMenuProps,
          highlightedIndex,
          inputValue,
          isOpen,
          selectedItem,
        }) => (
          <div className={classes.container}>
            {renderInput({
              fullWidth: true,
              classes,
              InputLabelProps: {
                style: {
                  color: 'white',
                  ...style
                }
              }, 
              InputProps: getInputProps({
                placeholder: label,
                style: style,
                onKeyDown: onKeyPress,
                onChange: () => {
                    setIsSelected(false)
                },
                onBlur: () => {
                    setIsBlured(false);
                },
                onFocus: () => {
                    setIsBlured(true);
                }
              }),
            })}
            <div {...getMenuProps()}>
              {(isOpen || showAllSuggestions) && !isSelected ? (
                <Paper className={classes.paper} square>
                  {getSuggestions(inputValue, suggestions, isBlured).map((suggestion, index) =>
                    renderSuggestion({
                      suggestion,
                      index,
                      itemProps: getItemProps({ item: suggestion.nombre }),
                      highlightedIndex,
                      selectedItem,
                    }),
                  )}
                </Paper>
              ) : null}
            </div>
          </div>
        )}
      </Downshift>
      <div className={classes.divider} />
      <div className={classes.divider} />
    </div>
  );
}

IntegrationDownshift.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(IntegrationDownshift);