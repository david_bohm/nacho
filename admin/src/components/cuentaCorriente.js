import React, { useState, useEffect, useRef } from 'react';
import conf from '../conf';
import axios from 'axios';
import Venta from './ventas';
import { Modal, Paper, Button, CircularProgress, Snackbar, TextField } from '@material-ui/core';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Input from './input';




const divModal = {
  minWidth: "500px",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  color: "black",
  backgroundColor: "white",
  padding: "20px",
  fontFamily: "Arial"
};

const modal = {
  color: "white",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  padding: "10px",

};

const divCuota = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",

};

const boton = {
  backgroundColor: "#2196f3",
  padding: "10px",
  marginTop: "10px",
};

const tablex = {
  border: "1px solid black",
  padding: "5px",
  align: "right"
}

const anular = {
  color: "white",
  padding: "5px",
  backgroundColor: "red"
}

const styleInput = {
  height: "35px",
  fontSize: "8px",
  padding: "0px"
}



const Pagos = ({ selectedCuota }) => {
  const mensaje = useRef('Por defecto');


  const elemento = useRef();
  const [pago, setPago] = useState(null);
  const [pagos, setPagos] = useState([]);
  const [totalPagos, setTotalPagos] = useState(0);
  const [interes, setInteres] = useState(null);
  const [interesValue, setInteresValue] = useState(null);
  const [pagoConInteres, setPagoConInteres] = useState(0);
  const [obs, setObs] = useState("");
  const [tipoRecargo, setTipoRecargo] = useState("ninguno");
  const [isLoading, setIsLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [isSaved, setIsSaved] = useState(false);

  const fechaVencCuota = selectedCuota.fechaVencimiento;

  const postPago = () => {
    setIsLoading(true);
    const pagoDatos = {
      fecha: new Date(),
      importe: pagoConInteres,
      cuotaId: selectedCuota.id,
      interes: interes,
      observaciones: obs
    }

    axios.post(`${conf.API_URL}/pagos`, pagoDatos)
      .then(response => {
        getPagos();
        setPago(null);         /*Pongo en cero todo despues de guardar un pago */
        setInteres(null);
        setPagoConInteres(null);
        setObs("");
        setInteresValue(null);
        setTipoRecargo("ninguno");
        setIsSaved(true)


        setTimeout(() => {
          setIsLoading(false);
        }, 2000)

      })
      .catch(err => {
        console.log(err);
      });
  }

  const getPagos = () => {
    axios.get(`${conf.API_URL}/cuotas/${selectedCuota.id}/pagos`)
      .then(response => {
        setPagos(response.data);
      })
      .catch(err => {
        console.log(err);
      });
  }

  const anularPago = (ultimoPago) => {

    let borra = window.confirm("Confirma borrado?");
    if (borra == true) {

      axios.delete(`${conf.API_URL}/pagos/${ultimoPago}`)
        .then(response => {

          getPagos();

        })
        .catch(err => {
          console.log(err);
        });
      }
  }


  const aplicaInteres = (interesInput) => {
    setInteresValue(interesInput);
    let _conRecargo = 0;


    if (tipoRecargo == "monto") {
      _conRecargo = parseFloat(pago) + parseFloat(interesInput);

    } else {
      _conRecargo = (parseFloat((pago * interesInput) / 100)) + parseFloat(pago);

    }

    setPagoConInteres(_conRecargo);
  }

  const inputPago = (x) => {
    console.log(x)
    if (isSaved) {
      setIsSaved(false);
    }
    
    setPago(x);
    setPagoConInteres(x);
  }

  

  useEffect(() => {
    getPagos();
  }, [])


  useEffect(() => {                                        /*verifica si esta paga */
    if (selectedCuota.importe <= totalPagos) {
      mensaje.current = "Cuota cancelada"
      setOpen(true);

    }
  }, [totalPagos])

  useEffect(() => {                                         /*verifica si el pago es mayor a la cuota */
    if ((selectedCuota.importe - totalPagos) < pago) {
      mensaje.current = "El pago es mayor al saldo"
      setOpen(true);

      setPago(selectedCuota.importe - totalPagos);                /* -------------- al ser mayor el pago al saldo, pone como pago el saldo */
      setPagoConInteres(selectedCuota.importe - totalPagos);

    }
  }, [pagoConInteres])


  useEffect(() => {
    let _totalPagos = 0;

    pagos.forEach(p => {

      _totalPagos += p.importe;

    })
    setTotalPagos(_totalPagos);
  }, [pagos])

  useEffect(() => {
    setInteres(pagoConInteres - pago);
  }, [pagoConInteres]);

  console.log(selectedCuota.montoTotal, totalPagos);

  let estado = "A termino";
  if (new Date(selectedCuota.fechaVencimiento).toLocaleDateString() < new Date(2020,11,17)) {
    estado = "Vencida";
  }

  /*const handleClick = () => {
    setOpen(true);
  };*/

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  
  return (

    <Paper>


      <div className="modal">
        <div className="datoscuota">
          <div className="txtmodal">Cuota nº :{selectedCuota.numeroCuota} {selectedCuota.fechaVencimiento}</div>

          <div className="txtmodal">Vencimiento :{`${new Date(selectedCuota.fechaVencimiento).toLocaleDateString()}`}</div>

          <div className="txtmodal">Estado : {estado}</div>


        </div>
        <div className="importes">
          <div className="columns-2 w-row">
            <div className="w-col w-col-6">
              <div className="txtmodal">Importe actualizado :</div>
            </div>
            <div className="column-14 w-col w-col-6">
              <div className="txtmodal">
                {new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(selectedCuota.importe)}
              </div>
            </div>
          </div>
        </div>
        <h4 className="heading-5">Pagos</h4>
        <div className="datospagos">
          <div className="datopago">
            <div className="txtmodal centrado">Fecha pago</div>
          </div>
          <div className="datopago">
            <div className="txtmodal centrado">Recargo</div>
          </div>
          <div className="datopago">
            <div className="txtmodal centrado">Observaciones</div>
          </div>
          <div className="datopago" />
        </div>

        {
          pagos.map((pago, index) =>
            <div className="datospagos blanco">
              {/*<div title="Eliminar pago" className="anular" onClick={(e) => anularPago(pago.id, e)}>X</div>*/}
              <div className="datopago">
                <div className="txtmodal centrado">{`${new Date(pago.fecha).toLocaleDateString()}`}</div>
              </div>
              <div className="datopago">
                <div className="txtmodal derecha"> {`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(pago.interes)}`}</div>
              </div>
              <div className="datopago">
                <div className="txtmodal centrado">{pago.observaciones}</div>
              </div>
              <div className="datopago">
                <div className="txtmodal derecha">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(pago.importe)}`}</div>
                
              </div>
              
              {
                pagos.length - 1 == index ?

                  <div title="Eliminar pago" className="anular" onClick={(e) => anularPago(pago.id, e)} />


                  : <div  className="noAnular" />

              }
              
              
            </div>
          )
        }

        <div className="totalpagos">
          <div className="txtmodal" ref={elemento}>Total pagos :</div>
          <div className="txtmodal">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(totalPagos)}`}</div>
        </div>
        <div className="totalpagos">
          <div className="txtmodal">Saldo :</div>
          <div className="txtmodal">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(selectedCuota.importe - totalPagos)}`}</div>
        </div>
        <div className="ingresapago">
          <h4 className="heading-5">Registrar pago</h4>
          <div className="w-form">
            <form id="email-form" name="email-form" data-name="Email Form">
              <div className="columns-3 w-row">
                <div className="column-16 w-col w-col-6">
                  <div className="input">
                    <div className="txtmodal">Importe :</div>

                    <Input  pay={pago} setVar={inputPago} tipo={'numDec'} /> 
                    

                  </div>
                  <div className="recargo">
                    <div className="txtmodal">Recargo :</div>

                    <div className="txtmodal">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(interes)}`}</div>


                  </div>

                  <div className="importefinal">
                    <div className="txtmodal">Importe final :</div>
                    <div className="div-block-17">
                      <div className="txtmodal">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(pagoConInteres)}`}</div>
                    </div>
                  </div>

                  <div className="input">
                    <div className="txtmodal">Observac.:</div>

                    <Input  pay={obs} setVar={setObs} tipo={'texto'} /> 
                    

                  </div>
                </div>

                

                


                <div className="column-17 w-col w-col-6">

                  {/*--------------------------------------------------------------------- Prueba de input-------------------------------------------- */}


                  <div className="radiobuttons">
                    <div onChange={e => setTipoRecargo(e.target.value)}>
                      <div className="txtmodal centrado">Recargo</div>
                      <label className="radiobuttonfield w-radio">
                        <input type="radio" data-name="Radio" id="radio" name="radio" checked={tipoRecargo === "ninguno" ? true : false} value="ninguno" required className="w-form-formradioinput w-radio-input" defaultChecked /><span className="tadiobuttonlabel w-form-label">Ninguno</span></label>
                      <label className="radiobuttonfield w-radio">
                        <input type="radio" data-name="Radio" id="radio" name="radio" checked={tipoRecargo === "monto" ? true : false} value="monto" className="w-form-formradioinput w-radio-input" /><span className="tadiobuttonlabel w-form-label">Monto</span></label>
                      <label className="radiobuttonfield w-radio">
                        <input type="radio" data-name="Radio" id="radio" name="radio" checked={tipoRecargo === "porcentaje" ? true : false} value="porcentaje" className="w-form-formradioinput w-radio-input" /><span className="tadiobuttonlabel w-form-label">Porcentaje</span></label>

                    </div>

                    {
                      tipoRecargo !== "ninguno" && pago !== null && pago !== "" ?
                      
                        <Input  pay={interesValue} setVar={aplicaInteres} tipo={'numDec'} />

                        
                        : null

                    }
                  </div>
                </div>

              </div>
            </form>

          </div>
        </div>


        <div className="div-block-16">
          <Button variant="contained" disabled={selectedCuota.importe < pago || isLoading || pagoConInteres == 0 || isSaved} color="primary" onClick={postPago}>
            {
              isLoading ? <CircularProgress /> : 'Guardar'
            }
          </Button>
        </div>


        <div>

        </div>


        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          open={open}
          autoHideDuration={6000}
          onClose={handleClose}
          message={mensaje.current}
          action={
            <React.Fragment>
              <Button color="secondary" size="small" onClick={handleClose}>
                UNDO
              </Button>
              <Button size="small" aria-label="close" color="inherit" onClick={handleClose}>

              </Button>
            </React.Fragment>
          }
        />


      </div>



    </Paper>
  )
};



const CuentaCorriente = props => {

  new Date(2020, 7, 1);  /*cambia fecha del sistema, para pruebas después sacarlo */



  const [cliente, setCliente] = useState({});

  const [ventas, setVentas] = useState([]);
  const [total, setTotal] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [selectedCuota, setSelectedCuota] = useState({});

  const getCliente = () => {
    axios.get(`${conf.API_URL}/clientes/${props.match.params.id}`)
      .then(response => {
        setCliente(response.data);

      })
      .catch(err => {
        console.log(err);
      });
  }

  const getVentas = () => {
    axios.get(`${conf.API_URL}/ventas`, {
      params: {
        filter: {
          where: {
            clienteId: props.match.params.id
          }
        }
      }
    })
      .then(response => {

        let _total = 0;

        response.data.forEach(venta => {
          _total += venta.total;
        });

        setTotal(_total);
        setVentas(response.data);

      })
      .catch(err => {
        console.log(err);
      });
  }

  const onCuotaClick = cuota => () => {
    setShowModal(true);
    setSelectedCuota(cuota);

  }

  const estaPagaCuota = () => {     /*-------marca la cuota como cancelada */


  }

  useEffect(() => {
    getCliente();
    getVentas();

  }, []);

  return (
    <>
      <div className="datoscliente">
        <div className="w-row">
          <div className="column-6 w-col w-col-4">
            <div className="txtcliente">Cliente:&nbsp; </div>
            <div className="txtcliente">{cliente.nombre} {cliente.apellido}</div>
          </div>
          <div className="column-12 w-col w-col-4">
            <div className="txtcliente">Categoría:&nbsp;</div>
            <div className="txtcliente">{cliente.categoriaCliente}</div>
          </div>
          <div className="column-13 w-col w-col-4">
            <div className="txtcliente">Vendedor:&nbsp;</div>
  <div className="txtcliente">{cliente.vendedor?.nombre} {cliente.vendedor?.apellido}</div>
          </div>
          <div className="column-13 w-col w-col-4">
            <div className="txtcliente">Total:&nbsp;</div>
            <div className="txtcliente">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(total)}`}</div>
          </div>
        </div>
      </div>

      <Modal style={modal}
        open={showModal}
        onClose={() => {
          setShowModal(false)
        }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Pagos selectedCuota={selectedCuota} />
      </Modal>

      {
        ventas.map(venta => {
          return <Venta venta={venta} onClick={onCuotaClick} />
        })
      }
    </>
  )
};

export default CuentaCorriente;


