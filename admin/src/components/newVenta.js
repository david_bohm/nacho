import React, { useState, useEffect } from 'react';
import { TextField, Button } from '@material-ui/core';
import axios from 'axios';
//import Autocomplete from './autocomplete';

import conf from '../conf';
import Input from './input';
//import DatePicker from 'react-date-picker';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Snack from './snack';

console.log('ASDASDAS')

const myStyle = {
    backgroundColor: "#e0d9d9",
    padding: "10px",

};

const precioStyle = {
    fontSize: "18px",
    marginTop: "10px",

};

const articuloStyle = {
    display: "flex",
    justifyContent: "space-between",
    alignItems: 'center',


};

const articuloStyle2 = {
    display: "flex",
    justifyContent: "space-between",
    alignItems: 'center',
    backgroundColor: "white",
    marginTop: "20px",
};

const NewVenta = (props) => {
    console.log("=_=_=_=_=_=_=_=", props);
    const [productos, setProductos] = useState([]);
    const [productosSeleccionados, setProductosSeleccionados] = useState([]);
    const [productosLinkeados, setProductosLinkeados] = useState([]);
    const [total, setTotal] = useState(0);
    const [subTotal, setSubTotal] = useState(0);
    const [fecha, setFecha] = useState(new Date());
    const [clientes, setClientes] = useState('');
    const [cuotas, setCuotas] = useState(1);
    const [entrega, setEntrega] = useState(0);
    const [descuento, setDescuento] = useState(0);
    const [descuentoMonto, setDescuentoMonto] = useState(0);
    const [cantidad, setCantidad] = useState(0);
    const [producto, setProducto] = useState('');
    const [clienteSeleccionado, setClienteSeleccionado] = useState(null);
    const [vendedor, setVendedor] = useState('');
    const [notification, setNotification] = useState({
        message: '',
        open: false,
        severity: ''
    });

    //------------------------------------------------------ clientes

    const getClientes = () => {
        axios.get(`${conf.API_URL}/clientes`)
            .then(response => {
                console.log(response);
                setClientes(response.data);
            })
            .catch(err => {
                console.log(err);
            });
    }

    useEffect(() => {
        getClientes();
        //getProductosLinkeados();
    }, []);

    const handleNotification = (message, severity) => {
        setNotification({
            message,
            severity,
            open: true
        });
    }

    const crearVenta = () => {
        const ventaDatos = {
            fecha: new Date(),
            descuento: descuento,
            entrega: entrega,
            cuotas: cuotas,
            vendedorId: clienteSeleccionado.vendedorId,
            clienteId: clienteSeleccionado.id,
            observaciones: '',
            productos: productosSeleccionados,
            total: total
        }

        axios.post(`${conf.API_URL}/ventas`, ventaDatos)  
            .then(response => {
                
                console.log("oooooooooooooooooo", response.data);
                // productosSeleccionados.forEach(producto => {
                //     axios.post(`${conf.API_URL}/VentaProductos`, {
                //         ventaId: response.data.id,
                //         productoId: producto.id,
                //         cantidad: producto.cantidad
                //     })
                //         .then(result => {
                //             handleNotification("Venta creada", "success")
                //             setTimeout(() => {
                //                 window.location.reload()
                //             }, 3000)
                //         });
                // });
                window.location.href = "/#/ventas"
            })
            .catch(err => {
                console.log(err);
            });
    }

    const handleChangeCliente = customer => {
        const cliente = clientes.filter(p => p.nombre === customer.nombre)[0];

        setClienteSeleccionado(cliente);

        //getVendedor(cliente.id); ----------------------------------------lo desactive

        console.log("cliente seleccionado: ", cliente);

        // axios.put(`${conf.API_URL}/ventas/${props.record.id}/products/rel/${producto.id}`)
        //     .then(response => {
        //         axios.get(`${conf.API_URL}/ventas/${props.record.id}`)
        //             .then(respuesta => {
        //                 const venta = respuesta.data;

        //                 axios.patch(`${conf.API_URL}/ventas/${props.record.id}`, {
        //                     total: venta.total + Number(producto.precio)
        //                 })
        //                     .then(respuesta2 => {
        //                         console.log('Salio todo bien', respuesta2);
        //                     })
        //             })

        //         getProductosLinkeados();
        //     })
        //     .catch(err => {
        //         console.log(err);
        //     });


    }

    //----------------------------------------------------------------------------- productosLinkeados

    const getProductosLinkeados = () => {
        // axios.get(`${conf.API_URL}/ventas/${props.record.id}/products`)
        //     .then(response => {
        //         console.log(response);
        //         setProductosLinkeados(response.data);
        //     })
        //     .catch(err => {
        //         console.log(err);
        //     })
    }

    const getProductos = () => {
        axios.get(`${conf.API_URL}/products`)
            .then(response => {
                console.log(response);
                setProductos(response.data);
            })
            .catch(err => {
                console.log(err);
            });
    }

    useEffect(() => {
        getProductos();
        getProductosLinkeados();
    }, []);

    useEffect(() => {
        let _total = 0;
        let _subTotal = 0;
        let _descuentoMonto = 0;

        productosSeleccionados.forEach(p => {
            _total += Number(p.precio) * Number(p.cantidad);
        })

        _subTotal = _total;
        _descuentoMonto = (_total * Number(descuento)) / 100;
        _total -= _descuentoMonto + Number(entrega);


        setSubTotal(_subTotal);
        setTotal(_total);
        setDescuentoMonto(_descuentoMonto);
    }, [productosSeleccionados]);

    useEffect(() => {
        let _total = 0;
        let _subTotal = 0;
        let _descuentoMonto = 0;

        productosSeleccionados.forEach(p => {
            _total += Number(p.precio);

        })

        _subTotal = _total;
        _descuentoMonto = (_total * Number(descuento)) / 100;
        _total -= _descuentoMonto + Number(entrega);


        setSubTotal(_subTotal);
        setTotal(_total);
        setDescuentoMonto(_descuentoMonto);
    }, [descuento, entrega]);


    const handleChangeProducto = producto => {
        if (!producto) return

        const productoRepetido = productosSeleccionados.find((element) => element.id === producto.id);

        if (productoRepetido) {
            handleNotification('Producto existe', 'warning')
            return;
        }
        //const producto = productos.filter(p => p.nombre === suggestion)[0];

        setProductosSeleccionados([...productosSeleccionados, {...producto, cantidad: cantidad}]);

        // axios.put(`${conf.API_URL}/ventas/${props.record.id}/products/rel/${producto.id}`)
        //     .then(response => {
        //         axios.get(`${conf.API_URL}/ventas/${props.record.id}`)
        //             .then(respuesta => {
        //                 const venta = respuesta.data;

        //                 axios.patch(`${conf.API_URL}/ventas/${props.record.id}`, {
        //                     total: venta.total + Number(producto.precio)
        //                 })
        //                     .then(respuesta2 => {
        //                         console.log('Salio todo bien', respuesta2);
        //                     })
        //             })

        //         getProductosLinkeados();
        //     })
        //     .catch(err => {
        //         console.log(err);
        //     });



    }

    const goToPago = () => {
        window.location.href = `/#/pagos/create?venta_id=${props.record.id}`;
    }

    const anularProducto = (i) => {
        console.log("*****************", productosSeleccionados, i);
        const copia = [...productosSeleccionados];

        copia.splice(i, 1);
        setProductosSeleccionados(copia);
    }

    console.log("****************", notification);

    return (

        <div>
            <div className="columns-4 w-row">
                <div className="column-29 w-col w-col-6">
                    <div className="div-block-19">
                        <div className="txtventas">Fecha:</div>
                        <TextField
                            id="date"
                            variant='outlined'
                            type="date"
                            defaultValue={new Date()}
                            onChange={setFecha}
                            InputProps={{ style: { maxHeight: 35, fontSize: 14, width: 200 } }}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </div>
                    <div className="div-block-19">
                        <div className="txtventas">Cliente:</div>

                        <Autocomplete
                            id="combo-box-demo"
                            options={clientes}
                            getOptionLabel={(option) => option.nombre + " " + option.apellido}
                            size="small"
                            style={{ width: "75%" }}
                            onChange={(e, v) => {
                                console.log(e, v)
                                handleChangeCliente(v);
                            }}
                            renderInput={(params) => <TextField {...params} variant="outlined" />}
                        />

                        {/*<Autocomplete label="Selecciona un producto" suggestions={clientes} onChange={handleChangeCliente} key="nombre" showAllSuggestions/>*/}

                        {/* <Input pay={cliente} setVar={setCliente} tipo={'texto'} /> */}

                    </div>
                    <div className="div-block-19.izq">
                        <div className="txtventas">Vendedor: </div>
                        {clienteSeleccionado && clienteSeleccionado.vendedor.nombre + " " + clienteSeleccionado.vendedor.apellido}
                        <div className="txtventas"> |</div>
                        <div className="txtventas">Comisión: {clienteSeleccionado && clienteSeleccionado.vendedor.comision}</div>
                    </div>
                </div>
                <div className="column-30 w-col w-col-6">
                    <div className="div-block-19">
                        <div className="txtventas">Cantidad de cuotas:</div>
                        <Input pay={cuotas} setVar={setCuotas} tipo={'num'} />

                    </div>
                    <div className="div-block-19">
                        <div className="txtventas">Descuento (%):</div>
                        <Input pay={descuento} setVar={setDescuento} tipo={'num'} max={100} />

                    </div>
                    <div className="div-block-19">
                        <div className="txtventas">Entrega:</div>
                        <Input pay={entrega} setVar={setEntrega} tipo={'numDec'} />

                    </div>
                </div>
            </div>
            <div className="encabezadoventas">
                <div className="divventa">
                    <div className="cantidad">
                        <div className="txtventas blanco">Cantidad</div>
                    </div>
                    <div className="producto">
                        <div className="txtventas blanco">Producto</div>
                    </div>
                    <div className="descripcion">
                        <div className="txtventas blanco">Descripción</div>
                    </div>
                    <div className="precio">
                        <div className="txtventas blanco">Precio</div>
                    </div>
                    <div className="borrar" />
                </div>
            </div>
            <div className="div-block-20">

                {
                    productosSeleccionados.map((producto, index) =>

                        <div className="divventa">
                            <div className="cantidad">
                                <div className="txtventas">{producto.cantidad}<br /></div>
                            </div>
                            <div className="producto">
                                <div className="txtventas">{producto.nombre} {producto.tipo} rodado {producto.rodado} {producto.color} <br /></div>
                            </div>
                            <div className="descripcion">
                                <div className="txtventas">{producto.descripcion}</div>
                            </div>
                            <div className="precio">
                                <div className="txtventas der">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(producto.precio)}`}</div>
                            </div>
                            <div className="borrar" onClick={(e) => anularProducto(index)}><img src="images/eliminar.png" loading="lazy" alt="" className="image-4" /></div>
                        </div>

                    )
                }


            </div>
            <div className="divventa input">
                <div className="cantidad">
                    <Input pay={cantidad} setVar={setCantidad} tipo={'num'} />

                </div>
                <div className="producto">

                    <Autocomplete

                        id="combo-box-demo"
                        options={productos}
                        getOptionLabel={(option) => option.nombre + " " + option.tipo + " rodado " + option.rodado + " " + option.color + " " + option.descripcion}
                        onChange={(e, v) => {
                            handleChangeProducto(v);
                        }}
                        size="small"
                        style={{ width: "auto", height: 47, fontSize: 14 }}
                        renderInput={(params) => <TextField {...params} variant="outlined" />}
                    />

                    {/*<Autocomplete label="Selecciona un producto" suggestions={productos} onChange={handleChangeProducto} key="nombre" showAllSuggestions/>*/}

                </div>
                <div className="descripcion" />
                <div className="precio" />
                <div className="borrar" />
            </div>
            <div className="totaltotal">
                <div className="totales">
                    <div className="divventa">
                        <div className="cantidad">
                            <div className="txtventas">Subtotal:<br /></div>
                        </div>
                        <div className="producto" />
                        <div className="descripcion" />
                        <div className="precio">
                            <div className="txtventas der">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(subTotal)}`}</div>
                        </div>
                        <div className="borrar" />
                    </div>
                    <div className="divventa">
                        <div className="cantidad">
                            <div className="txtventas">Descuento:<br /></div>
                        </div>
                        <div className="producto" />
                        <div className="descripcion" />
                        <div className="precio">
                            <div className="txtventas der">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(descuentoMonto)}`}</div>
                        </div>
                        <div className="borrar" />
                    </div>
                    <div className="divventa">
                        <div className="cantidad">
                            <div className="txtventas">Entrega:<br /></div>
                        </div>
                        <div className="producto" />
                        <div className="descripcion" />
                        <div className="precio">
                            <div className="txtventas der">{`- ${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(entrega)}`}</div>
                        </div>
                        <div className="borrar" />
                    </div>
                </div>
            </div>
            <div className="div-block-25">
                <div className="divventa">
                    <div className="cantidad">
                        <div className="txtventas blanco">Total:</div>
                    </div>
                    <div className="producto" />
                    <div className="descripcion" />
                    <div className="precio">
                        <div className="txtventas blanco der">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(total)}`}</div>
                    </div>
                    <div className="borrar" />
                </div>
            </div>
            <div className="div-block-23">
                <div className="div-block-24">
                    <div className="txtventas">Cantidad de artículos: {productosSeleccionados.length}<br /></div>
                    {
                        cuotas > 0 && total > 0 ?
                            <div className="txtventas">Financiación: {cuotas} cuotas de {`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(total / cuotas)}`}<br /></div>
                            : null
                    }
                </div><a className="button-3 w-button" onClick={crearVenta}>Guardar</a>
            </div>

            <Snack open={notification.open} message={notification.message} severity={notification.severity} />
        </div>


    )
}

export default NewVenta;

