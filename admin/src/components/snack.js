import React, { useState, useEffect } from 'react';
import Snackbar  from '@material-ui/core/Snackbar';
import { Alert } from '@material-ui/lab';

const Snack = ({ open, message, severity, handleClose }) => {
    const [snackOpen, setSnackOpen] = useState(open);

    useEffect(() => {
        setSnackOpen(open);
    }, [ open ])

    const _handleClose = () => {
        setSnackOpen(false);
    }

    return (
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          open={snackOpen}
          autoHideDuration={4500}
          onClose={handleClose ? handleClose : _handleClose} 
        >
            <Alert severity={severity} onClose={handleClose ? handleClose : _handleClose} >{message}</Alert>
        </Snackbar>

    )

};

export default Snack;    