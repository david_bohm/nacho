import React, { useState, useEffect } from 'react';
import { TextField, Button } from '@material-ui/core';
import axios from 'axios';
//import Autocomplete from './autocomplete';

import conf from '../conf';
import Input from './input';
//import DatePicker from 'react-date-picker';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Snack from './snack';

const Consignacion = (props) => {

    const [fecha, setFecha] = useState(new Date());

    return (

        <div>
            <div className="columns-4 w-row">
                <div className="column-29 w-col w-col-6">
                    <div className="div-block-19">
                        <div className="txtventas">Fecha:</div>
                        <TextField
                            id="date"
                            variant='outlined'
                            type="date"
                            defaultValue={new Date()}
                            onChange={setFecha}
                            InputProps={{ style: { maxHeight: 35, fontSize: 14, width: 200 } }}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </div>
                    <div className="div-block-19">
                        <div className="txtventas">Cliente:</div>

                        {/*<Autocomplete
                            id="combo-box-demo"
                            options={clientes}
                            getOptionLabel={(option) => option.nombre + " " + option.apellido}
                            size="small"
                            style={{ width: "75%" }}
                            onChange={(e, v) => {
                                console.log(e, v)
                                handleChangeCliente(v);
                            }}
                            renderInput={(params) => <TextField {...params} variant="outlined" />}
                        />

                        {/*<Autocomplete label="Selecciona un producto" suggestions={clientes} onChange={handleChangeCliente} key="nombre" showAllSuggestions/>*/}

                        {/* <Input pay={cliente} setVar={setCliente} tipo={'texto'} /> */}

                    </div>
                    <div className="div-block-19.izq">
                        <div className="txtventas">Vendedor: </div>
                        {"Pepe"}
                        <div className="txtventas"> |</div>
                    </div>
                </div>
                <div className="column-30 w-col w-col-6">
                    
                </div>
            </div>
            <div className="encabezadoventas">
                <div className="divventa">
                    <div className="cantidad">
                        <div className="txtventas blanco">Cantidad</div>
                    </div>
                    <div className="producto">
                        <div className="txtventas blanco">Producto</div>
                    </div>
                    <div className="descripcion">
                        <div className="txtventas blanco">Descripción</div>
                    </div>
                    <div className="precio">
                        <div className="txtventas blanco">Precio</div>
                    </div>
                    <div className="borrar" />
                </div>
            </div>
            <div className="div-block-20">

                {
                    productosSeleccionados.map((producto, index) =>

                        <div className="divventa">
                            <div className="cantidad">
                                <div className="txtventas">{producto.cantidad}<br /></div>
                            </div>
                            <div className="producto">
                                <div className="txtventas">{producto.nombre} {producto.tipo} rodado {producto.rodado} {producto.color} <br /></div>
                            </div>
                            <div className="descripcion">
                                <div className="txtventas">{producto.descripcion}</div>
                            </div>
                            <div className="precio">
                                <div className="txtventas der">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(producto.precio)}`}</div>
                            </div>
                            <div className="borrar" onClick={(e) => anularProducto(index)}><img src="images/eliminar.png" loading="lazy" alt="" className="image-4" /></div>
                        </div>

                    )
                }


            </div>
            <div className="divventa input">
                <div className="cantidad">
                    <Input pay={cantidad} setVar={setCantidad} tipo={'num'} />

                </div>
                <div className="producto">

                    <Autocomplete

                        id="combo-box-demo"
                        options={productos}
                        getOptionLabel={(option) => option.nombre + " " + option.tipo + " rodado " + option.rodado + " " + option.color + " " + option.descripcion}
                        onChange={(e, v) => {
                            handleChangeProducto(v);
                        }}
                        size="small"
                        style={{ width: "auto", height: 47, fontSize: 14 }}
                        renderInput={(params) => <TextField {...params} variant="outlined" />}
                    />

                    {/*<Autocomplete label="Selecciona un producto" suggestions={productos} onChange={handleChangeProducto} key="nombre" showAllSuggestions/>*/}

                </div>
                <div className="descripcion" />
                <div className="precio" />
                <div className="borrar" />
            </div>
            <div className="totaltotal">
                <div className="totales">
                    <div className="divventa">
                        <div className="cantidad">
                            <div className="txtventas">Subtotal:<br /></div>
                        </div>
                        <div className="producto" />
                        <div className="descripcion" />
                        <div className="precio">
                            <div className="txtventas der">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(subTotal)}`}</div>
                        </div>
                        <div className="borrar" />
                    </div>
                    <div className="divventa">
                        <div className="cantidad">
                            <div className="txtventas">Descuento:<br /></div>
                        </div>
                        <div className="producto" />
                        <div className="descripcion" />
                        <div className="precio">
                            <div className="txtventas der">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(descuentoMonto)}`}</div>
                        </div>
                        <div className="borrar" />
                    </div>
                    <div className="divventa">
                        <div className="cantidad">
                            <div className="txtventas">Entrega:<br /></div>
                        </div>
                        <div className="producto" />
                        <div className="descripcion" />
                        <div className="precio">
                            <div className="txtventas der">{`- ${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(entrega)}`}</div>
                        </div>
                        <div className="borrar" />
                    </div>
                </div>
            </div>
            <div className="div-block-25">
                <div className="divventa">
                    <div className="cantidad">
                        <div className="txtventas blanco">Total:</div>
                    </div>
                    <div className="producto" />
                    <div className="descripcion" />
                    <div className="precio">
                        <div className="txtventas blanco der">{`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(total)}`}</div>
                    </div>
                    <div className="borrar" />
                </div>
            </div>
            <div className="div-block-23">
                <div className="div-block-24">
                    <div className="txtventas">Cantidad de artículos: {productosSeleccionados.length}<br /></div>
                    {
                        cuotas > 0 && total > 0 ?
                            <div className="txtventas">Financiación: {cuotas} cuotas de {`${new Intl.NumberFormat("de-DE", { style: "currency", currency: "ARS" }).format(total / cuotas)}`}<br /></div>
                            : null
                    }
                </div><a className="button-3 w-button" onClick={crearVenta}>Guardar</a>
            </div>

            <Snack open={notification.open} message={notification.message} severity={notification.severity} />
        </div>


    )   
} 

export default Consignacion;