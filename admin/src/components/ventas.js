import React, { useState, useEffect } from 'react';
import conf from '../conf';
import axios from 'axios';
import Cuota from './cuota';

const Ventas = props => {
    const [cuotas, setCuotas] = useState([]);
    
    let current_datetime = new Date(props.venta.fecha);
    let formatted_date = current_datetime.getDate() + "/" + (current_datetime.getMonth() + 1) + "/" + current_datetime.getFullYear();

    const getCuotas = () => {
        axios.get(`${conf.API_URL}/cuotas`, {
            params: {
                filter: {
                    where: {
                        ventaId: props.venta.id
                    }
                } 
            }
        })
            .then(response => {
                console.log("++++",response);

                setCuotas(response.data);                
            })
            .catch(err => {
                console.log(err);
            });
    }

    useEffect(() => {
        getCuotas();
    }, []);
        

    return (
        <>
            
            
            <div className="encabezadocompra">
                <div className="w-row">
                    <div className="column-5 w-col w-col-2 w-col-medium-4 w-col-small-6 w-col-tiny-tiny-stack">
                        <div className="div-block-12">
                            <div className="txtcliente blanco">Fecha:</div>
                        </div>
                        <div className="txtcliente blanco gde">{formatted_date}</div>
                    </div>
                    <div className="column-7 w-col w-col-2 w-col-medium-4 w-col-small-6 w-col-tiny-tiny-stack">
                        <div className="div-block-12">
                            <div className="txtcliente blanco">Importe:</div>
                        </div>
                        <div className="txtcliente blanco gde">{props.venta.total}</div>
                    </div>
                    <div className="column-8 w-col w-col-2 w-col-medium-4 w-col-small-6 w-col-tiny-tiny-stack">
                        <div className="div-block-12">
                            <div className="txtcliente blanco">Importe act:</div>
                        </div>
                        <div className="txtcliente blanco gde">0</div>
                    </div>
                    <div className="column-9 w-col w-col-2 w-col-medium-4 w-col-small-6 w-col-tiny-tiny-stack">
                        <div className="div-block-12">
                            <div className="txtcliente blanco">Total pagos:</div>
                        </div>
                        <div className="txtcliente blanco gde">0</div>
                    </div>
                    <div className="column-10 w-col w-col-2 w-col-medium-4 w-col-small-6 w-col-tiny-tiny-stack">
                        <div className="div-block-12">
                            <div className="txtcliente blanco">Saldo:</div>
                        </div>
                        <div className="txtcliente blanco gde">0</div>
                    </div>
                    <div className="column-11 w-col w-col-2 w-col-medium-4 w-col-small-6 w-col-tiny-tiny-stack">
                        <div className="div-block-12">
                            <div className="txtcliente blanco">Otro:</div>
                        </div>
                        <div className="txtcliente blanco" />
                    </div>
                </div>
            </div>
            
            <div className="wrapperctacte">
                <div className="contenido">
                    <div className="encabezadopago">
                        <div className="tituloencabezadopago">
                            <div className="txtcliente">Cuota</div>
                        </div>
                        <div className="tituloencabezadopago">
                            <div className="txtcliente">Fecha venc</div>
                        </div>
                        <div className="tituloencabezadopago">
                            <div className="txtcliente">Fecha pago</div>
                        </div>
                        <div className="tituloencabezadopago">
                            <div className="txtcliente">Importe orig</div>
                        </div>
                        <div className="tituloencabezadopago">
                            <div className="txtcliente">Importe actualizado</div>
                        </div>
                        <div className="tituloencabezadopago">
                            <div className="txtcliente">Total pagado</div>
                        </div>
                        <div className="tituloencabezadopago">
                            <div className="txtcliente">Saldo cuota</div>
                        </div>
                    </div>
                    
                    {
                        cuotas.map(cuota => 
                            <Cuota info={cuota} getCuotas={getCuotas} onClick={props.onClick}/>                       
                        )
                    }
                </div>
            </div>


            
        </>
    )
};

export default Ventas;
