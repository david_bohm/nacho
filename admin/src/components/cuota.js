import React, { useState, useEffect } from 'react';
import conf from '../conf';
import axios from 'axios';

const Cuota = props => {
    /*const fecha = props.venta.fecha;
    let options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
    const fechaFormat = fecha.toLocaleDateString("es-ES", options);*/

    let claseCuota = "datospago verde";

    return (
        <>
                <div className="contenido" onClick={props.onClick(props.info)}>
                    <div className="pago">
    
                        <div className={claseCuota}>
                            <div className="txtcliente cuota verde">{props.info.numeroCuota}</div>
                        </div>
                        
                                                
                        <div className="datospago">
                            <div className="txtcliente">{`${new Date(props.info.fechaVencimiento).toLocaleDateString()}`}</div>
                        </div>
                        <div className="datospago">
                            <div className="txtcliente">15/09/20</div>
                        </div>
                        <div className="datospago">
                            <div className="txtcliente importe">{new Intl.NumberFormat("de-DE", {style: "currency", currency: "ARS"}).format(props.info.importe)}</div>
                        </div>
                        <div className="datospago">
                            <div className="txtcliente importe">{new Intl.NumberFormat("de-DE", {style: "currency", currency: "ARS"}).format(props.info.montoActualizado)}</div>
                        </div>
                        <div className="datospago">
                            <div className="txtcliente importe">{new Intl.NumberFormat("de-DE", {style: "currency", currency: "ARS"}).format(0)}</div>
                        </div>
                        <div className="datospago">
                            <div className="txtcliente importe">{new Intl.NumberFormat("de-DE", {style: "currency", currency: "ARS"}).format(0)}</div>
                        </div>
                    </div>
                    
                </div>

            
            {/*
                ventas.map(venta => 
                    <div>Compra:  {venta.fecha} interés: {venta.interes} cuotas: {venta.cuotas} total: {venta.total}  ------------ {venta.vendedorId}</div>
                )*/
            }
        </>
    )
};

export default Cuota;
