import React, { useState, useEffect, useRef } from 'react';
import conf from '../conf';
import { Modal, Paper, Button, CircularProgress, Snackbar, TextField } from '@material-ui/core';
import CurrencyTextField from '@unicef/material-ui-currency-textfield';
import NumberFormat from 'react-number-format';
//import MaskedInput from 'react-text-mask';


const Input = props => {
    
    switch (props.tipo) {
        case 'texto':
          return <TextField  variant="outlined" InputProps={{style: {maxHeight: 35, width: "auto" , fontSize: 14}}} onChange={e => props.setVar(e.target.value)}/>;
        case 'numDec':
          return <NumberFormat value={props.pay} decimalScale={2} customInput={TextField} variant='outlined' InputProps={{style: {maxHeight: 35, fontSize: 14}}} inputProps={{style: {textAlign: 'right'}}} thousandSeparator={false} prefix={''} onChange={e => props.setVar(e.target.value)}/>;
        case 'num':
          return <NumberFormat value={props.pay}  maximumValue={100} customInput={TextField} variant='outlined' InputProps={{style: {maxHeight: 35, fontSize: 14}}} inputProps={{style: {textAlign: 'right'}}} thousandSeparator={false} prefix={''} onChange={e => props.setVar(e.target.value)}/>;
        default:
          return null;
    }
        
    /*return (
        <>   
            {   
                props.tipo ? 
                    <NumberFormat value={props.pay} decimalScale={2} customInput={TextField} variant='outlined' InputProps={{style: {maxHeight: 35, fontSize: 14}}} inputProps={{style: {textAlign: 'right'}}} thousandSeparator={false} prefix={''} onChange={e => props.setVar(e.target.value)}/>
                :
                    <TextField  variant="outlined" InputProps={{style: {maxHeight: 35, fontSize: 14}}} onChange={e => props.setVar(e.target.value)}/>
            }
        </>  
    )*/
};

export default Input;