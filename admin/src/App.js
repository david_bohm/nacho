import React from 'react';
import { render } from 'react-dom';
import { Admin, Resource } from 'react-admin';
import loopbackRestClient, {authClient} from 'aor-loopback';
import polyglotI18nProvider from 'ra-i18n-polyglot';
import spanishMessages from '@blackbox-vision/ra-language-spanish';
import interceptor from './interceptor';

// MODELOS
import { ClienteList, ClienteCreate, ClienteEdit } from './resources/cliente';
import { ProductoList, ProductoCreate, ProductoEdit } from './resources/producto';
import { VendedorList, VendedorCreate, VendedorEdit } from './resources/vendedor';
//import { VentaList, VentaCreate, VentaEdit } from './resources/venta';
import { VentaList, VentaCreate } from './resources/venta';
import { Route } from 'react-router-dom';
import CuentaCorriente from './components/cuentaCorriente';
import NewVenta  from './components/newVenta';
import VentaDetail from './components/ventaDetail';

import Consignacion from './components/consignacion';

import conf from './conf';

const customRoutes = [
  <Route exact path="/clientes/:id/cuentaCorriente" component={CuentaCorriente} />,
  <Route exact path="/ventas/:id/show" component={VentaDetail} />
]

const restClient = interceptor(loopbackRestClient(conf.API_URL));
const authProvider = authClient(conf.AUTH_URL);

const messages = {
  'es': spanishMessages,
};

const i18nProvider = polyglotI18nProvider(locale => messages['es']);

const App = () =>
    <Admin dataProvider={restClient} locale="es" i18nProvider={i18nProvider}  customRoutes={customRoutes}>
        <Resource name="clientes" list={ClienteList} create={ClienteCreate} edit={ClienteEdit}/>
        <Resource name="products" list={ProductoList} create={ProductoCreate} edit={ProductoEdit}/>
        <Resource name="vendedores" list={VendedorList} create={VendedorCreate} edit={VendedorEdit}/>
        {/*<Resource name="ventas" list={VentaList} create={VentaCreate} edit={VentaEdit}/>*/}
        <Resource name="ventas" list={VentaList} create={NewVenta} />
        <Resource name="consignaciones"  create={Consignacion} />
        
    </Admin>

export default App;
