import React from 'react';
import axios from 'axios';
import RichTextInput from 'ra-input-rich-text';


import {
    List,
    Datagrid,
    TextField,
    NumberField,
    DateField,
    DateTimeField,
    BooleanField,
    RichTextField,
    ChoiceField,
    ReferenceField,
    Filter
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    RadioButtonGroupInput,
    Edit,
    SimpleForm,
    DateInput,
    DisabledInput,
    AutocompleteInput,
    TextInput,
    ReferenceInput,
    SelectInput,
    NumberInput,
    BooleanInput,
    ArrayInput,
    SimpleFormIterator,
    ImageInput,
    ImageField
} from 'react-admin';
import {
    Create,
    SaveButton,
    Toolbar,
    required
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

import MyImageField from '../imageField';
import Consignacion from '../components/consignacion';

//import RichTextInput from 'ra-input-rich-text';

import conf from '../conf';

{/*const onFileSelected = async e => {
    console.log('file', e)
    const formData = new FormData();

        formData.append('image', e[0]);

        try {
            const upload = await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
                headers: {
                'Content-Type': 'multipart/form-data'
            }
        });

        console.log('upload success', upload);

    } catch (error) {
        console.log('error', error);
    }
}

const onFilesSelected = async e => {
    console.log('files', e)

    try {
        const result = await Promise.all(
            e.map(async file => {
                const formData = new FormData;

                formData.append('file', file);

                return await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                });
            })
        );

        console.log('FINAL', result);
    } catch (error) {
        console.log('error', error);
    }
}

const EditMe = (props) => (
    <div onClick={() => { console.log(props); window.location.href = "/#/ventas/" + props.record.id + "/show"}}>EDITAR</div>
)


export const VentaList = (props) => {
    return (
        <List {...props} title="SISTEMA DE GESTION | Lista de Ventas">
            <Datagrid>
                <DateField source="fecha" label="Fecha"/>
                                                
                <EditMe/>
            </Datagrid>
        </List>
    )
}*/}

export const ConsignacionCreate = (props) => {
    console.log(props);

    return (
        <Create {...props} title="SISTEMA DE GESTION | Consignación a vendedores" >
            {/*<SimpleForm variant="standard">
                
                <DateInput source="fecha" label="Fecha"/>

                <ReferenceInput label="Cliente" source="clienteId" reference="clientes" validate={[required()]}>
                    <SelectInput optionText="nombre" />
                </ReferenceInput>

                <ReferenceInput label="Vendedor" source="vendedorId" reference="vendedores" validate={[required()]}>
                    <SelectInput optionText="nombre" />
                </ReferenceInput>

                <TextInput source="interes" label="Interés"></TextInput>
                <TextInput source="cuotas" label="Cuotas"></TextInput>
            </SimpleForm>*/}

            <Consignacion />

        </Create>
    )
}

{/*export const VentaEdit = (props) => {
    return (
        <Edit {...props} title="SISTEMA DE GESTION | Editar Venta" >
            
        </Edit>
    )
}*/}