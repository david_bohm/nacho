import React from 'react';
import axios from 'axios';
import RichTextInput from 'ra-input-rich-text';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    DateField,
    DateTimeField,
    BooleanField,
    RichTextField,
    ChoiceField,
    ReferenceField,
    Filter
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    RadioButtonGroupInput,
    Edit,
    SimpleForm,
    DateInput,
    DisabledInput,
    AutocompleteInput,
    TextInput,
    ReferenceInput,
    SelectInput,
    NumberInput,
    BooleanInput,
    ArrayInput,
    SimpleFormIterator,
    ImageInput,
    ImageField
} from 'react-admin';
import {
    Create,
    SaveButton,
    Toolbar,
    required
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

import MyImageField from '../imageField';

//import RichTextInput from 'ra-input-rich-text';

import conf from '../conf';

const onFileSelected = async e => {
    console.log('file', e)
    const formData = new FormData();

        formData.append('image', e[0]);

        try {
            const upload = await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
                headers: {
                'Content-Type': 'multipart/form-data'
            }
        });

        console.log('upload success', upload);

    } catch (error) {
        console.log('error', error);
    }
}

const onFilesSelected = async e => {
    console.log('files', e)

    try {
        const result = await Promise.all(
            e.map(async file => {
                const formData = new FormData;

                formData.append('file', file);

                return await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                });
            })
        );

        console.log('FINAL', result);
    } catch (error) {
        console.log('error', error);
    }
}



export const ProductoList = (props) => {
    return (
        <List {...props} title="SISTEMA DE GESTION | Lista de Productos">
            <Datagrid>
                <TextField source="nombre" label="Nombre"/>
                <TextField source="tipo" label="Tipo"/>
                
                <TextField source="rodado" label="Rodado"/>
                <TextField source="color" label="Color"/>
                <TextField source="precio" label="Precio"/>
                <TextField source="stock" label="Stock"/>
                <TextField source="descripcion" label="Descripción"/>
                                
                <EditButton/>
            </Datagrid>
        </List>
    )
}

export const ProductoCreate = (props) => {
    return (
        <Create {...props} title="SISTEMA DE GESTION | Crear Categoria" >
            <SimpleForm variant="standard">
                
                <SelectInput source="nombre" choices={[
                    { id: 'bicicleta', name: 'Bicicleta' },
                    { id: 'otro', name: 'Otro' },
                ]} />
                
                <SelectInput source="tipo" choices={[
                    { id: 'paseo', name: 'Paseo' },
                    { id: 'cross', name: 'Cross' },
                    { id: 'mountainBike', name: 'Mountain bike' },
                    { id: 'crossFreestyle', name: 'Cross freestyle' },
                    { id: 'tentacion', name: 'Tentación' },
                    { id: 'playeraFullDama', name: 'Playera full dama' },
                    { id: 'playeraFullNino', name: 'Playera full niño' },
                    { id: 'playeraFullVaron', name: 'Playera full varón' },
                    { id: 'primavera', name: 'Primavera' },
                    { id: 'lady', name: 'Lady' },
                ]} />
                
                <SelectInput source="rodado" choices={[
                    { id: '12', name: '12' },
                    { id: '14', name: '14' },
                    { id: '16', name: '16' },
                    { id: '20', name: '20' },
                    { id: '22', name: '22' },
                    { id: '24', name: '24' },
                    { id: '26', name: '26' },
                    { id: '29', name: '29' },
                ]} />

                <SelectInput source="color" choices={[
                    { id: 'blanca', name: 'Blanca' },
                    { id: 'roja', name: 'Roja' },
                    { id: 'negra', name: 'Negra' },
                    { id: 'azul', name: 'Azul' },
                    { id: 'verde', name: 'Verde' },
                    { id: 'amarilla', name: 'Amarilla' },
                    { id: 'marron', name: 'Marrón' },
                    { id: 'rosa', name: 'Rosa' },
                    { id: 'naranja', name: 'Naranja' },
                ]} />

                <TextInput source="precio" label="Precio"/>

                <TextInput source="stock" label="Stock"/>
              
                <TextInput source="descripcion" label="Descripción"/>
                              
            </SimpleForm>
        </Create>
    )
}

export const ProductoEdit = (props) => {
    console.log(props);
    return (
        <Edit {...props} title="SISTEMA DE GESTION | Editar Categoria" >
            <SimpleForm variant="standard">
            <SelectInput source="nombre" choices={[
                    { id: 'bicicleta', name: 'Bicicleta' },
                    { id: 'otro', name: 'Otro' },
                ]} />
                
                <SelectInput source="tipo" choices={[
                    { id: 'paseo', name: 'Paseo' },
                    { id: 'cross', name: 'Cross' },
                    { id: 'mountainBike', name: 'Mountain bike' },
                    { id: 'crossFreestyle', name: 'Cross freestyle' },
                    { id: 'tentacion', name: 'Tentación' },
                    { id: 'playeraFullDama', name: 'Playera full dama' },
                    { id: 'playeraFullNino', name: 'Playera full niño' },
                    { id: 'playeraFullVaron', name: 'Playera full varón' },
                    { id: 'primavera', name: 'Primavera' },
                    { id: 'lady', name: 'Lady' },
                ]} />
                
                <SelectInput source="rodado" choices={[
                    { id: '12', name: '12' },
                    { id: '14', name: '14' },
                    { id: '16', name: '16' },
                    { id: '20', name: '20' },
                    { id: '22', name: '22' },
                    { id: '24', name: '24' },
                    { id: '26', name: '26' },
                    { id: '29', name: '29' },
                ]} />

                <SelectInput source="color" choices={[
                    { id: 'blanca', name: 'Blanca' },
                    { id: 'roja', name: 'Roja' },
                    { id: 'negra', name: 'Negra' },
                    { id: 'azul', name: 'Azul' },
                    { id: 'verde', name: 'Verde' },
                    { id: 'amarilla', name: 'Amarilla' },
                    { id: 'marron', name: 'Marrón' },
                    { id: 'rosa', name: 'Rosa' },
                    { id: 'naranja', name: 'Naranja' },
                ]} />

                <TextInput source="precio" label="Precio"/>

                <TextInput source="stock" label="Stock"/>
              
                <TextInput source="descripcion" label="Descripción"/>
                           
            </SimpleForm>
        </Edit>
    )
}
