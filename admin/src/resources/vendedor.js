import React from 'react';
import axios from 'axios';
import RichTextInput from 'ra-input-rich-text';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    DateField,
    DateTimeField,
    BooleanField,
    RichTextField,
    ChoiceField,
    ReferenceField,
    Filter
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    RadioButtonGroupInput,
    Edit,
    SimpleForm,
    DateInput,
    DisabledInput,
    AutocompleteInput,
    TextInput,
    ReferenceInput,
    SelectInput,
    NumberInput,
    BooleanInput,
    ArrayInput,
    SimpleFormIterator,
    ImageInput,
    ImageField
} from 'react-admin';
import {
    Create,
    SaveButton,
    Toolbar,
    required
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

import MyImageField from '../imageField';

//import RichTextInput from 'ra-input-rich-text';
import conf from '../conf';


const onFileSelected = async e => {
    console.log('file', e)
    const formData = new FormData();

        formData.append('image', e[0]);

        try {
            const upload = await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
                headers: {
                'Content-Type': 'multipart/form-data'
            }
        });

        console.log('upload success', upload);

    } catch (error) {
        console.log('error', error);
    }
}

const onFilesSelected = async e => {
    console.log('files', e)

    try {
        const result = await Promise.all(
            e.map(async file => {
                const formData = new FormData;

                formData.append('file', file);

                return await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                });
            })
        );

        console.log('FINAL', result);
    } catch (error) {
        console.log('error', error);
    }
}



export const VendedorList = (props) => {
    return (
        <List {...props} title="SISTEMA DE GESTION | Lista de Vendedores">
            <Datagrid>
                <TextField source="nombre" label="Nombre"/>
                <TextField source="apellido" label="Apellido"/>
                <TextField source="dni" label="DNI"/>
                <TextField source="direccion" label="Dirección"/>
                <TextField source="telefono" label="Teléfono"/>
                <TextField source="mail" label="Mail"/>
                <TextField source="zona" label="Zona"/>
                <TextField source="comision" label="Comisión"/>                               
                <EditButton/>
            </Datagrid>
        </List>
    )
}

export const VendedorCreate = (props) => {
    return (
        <Create {...props} title="SISTEMA DE GESTION | Crear Vendedor" >
            <SimpleForm variant="standard">
                
                <TextInput source="nombre" label="Nombre"/>
                <TextInput source="apellido" label="Apellido"/>
                <TextInput source="dni" label="DNI"/>
                <TextInput source="direccion" label="Dirección"/>
                <TextInput source="telefono" label="Teléfono"/>
                <TextInput source="mail" label="Mail"/>
                <TextInput source="zona" label="Zona"/>
                <TextInput source="comision" label="Comisión"/> 
                                              
            </SimpleForm>
        </Create>
    )
}

export const VendedorEdit = (props) => {
    console.log(props);
    return (
        <Edit {...props} title="SISTEMA DE GESTION | Editar Vendedor" >
            <SimpleForm variant="standard">
            
                <TextInput source="nombre" label="Nombre"/>
                <TextInput source="apellido" label="Apellido"/>
                <TextInput source="dni" label="DNI"/>
                <TextInput source="direccion" label="Dirección"/>
                <TextInput source="telefono" label="Teléfono"/>
                <TextInput source="mail" label="Mail"/>
                <TextInput source="zona" label="Zona"/>
                <TextInput source="comision" label="Comisión"/> 
                                          
            </SimpleForm>
        </Edit>
    )
}
