import React from 'react';
import axios from 'axios';
import RichTextInput from 'ra-input-rich-text';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    DateField,
    DateTimeField,
    BooleanField,
    RichTextField,
    ChoiceField,
    ReferenceField,
    Filter
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    RadioButtonGroupInput,
    Edit,
    SimpleForm,
    DateInput,
    DisabledInput,
    AutocompleteInput,
    TextInput,
    ReferenceInput,
    SelectInput,
    NumberInput,
    BooleanInput,
    ArrayInput,
    SimpleFormIterator,
    ImageInput,
    ImageField
} from 'react-admin';
import {
    Create,
    SaveButton,
    Toolbar,
    required
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

import MyImageField from '../imageField';

//import RichTextInput from 'ra-input-rich-text';

import conf from '../conf';

const onFileSelected = async e => {
    console.log('file', e)
    const formData = new FormData();

        formData.append('image', e[0]);

        try {
            const upload = await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
                headers: {
                'Content-Type': 'multipart/form-data'
            }
        });

        console.log('upload success', upload);

    } catch (error) {
        console.log('error', error);
    }
}

const onFilesSelected = async e => {
    console.log('files', e)

    try {
        const result = await Promise.all(
            e.map(async file => {
                const formData = new FormData;

                formData.append('file', file);

                return await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                });
            })
        );

        console.log('FINAL', result);
    } catch (error) {
        console.log('error', error);
    }
}

export const PagoList = (props) => {
    return (
        <List {...props} title="SISTEMA DE GESTION | Lista de Pagos">
            <Datagrid>
                
                <ReferenceField label="Venta" source="clienteId" reference="clientes" allowEmpty={true}>
                    <TextField source="nombre" />
                </ReferenceField>
                
                <DateField source="fecha" label="Fecha"/>
                <TextField source="importe" label="Importe"/> 
                <TextField source="observaciones" label="Observaciones"/>
                                               
                <EditButton/>
            </Datagrid>
        </List>
    )
}

export const PagoCreate = (props) => {
    console.log(props, window.location);

    const a = window.location.href.split('=');

    console.log('a', a);

    let defaultValues = {}
  
    if (a[1]) {
        console.log('EHHHH')
        defaultValues = {
            ventaId: a[1],
        }
    }

    console.log(defaultValues);

    return (
        <Create {...props} title="SISTEMA DE GESTION | Crear Pago" >
            <SimpleForm variant="standard" initialValues={defaultValues}>
                <DateInput source="fecha" label="Fecha"/>
                <TextInput source="importe" label="Importe"/>
                <TextInput source="observaciones" label="Observaciones"/>

                {/*<ReferenceInput label="Cliente" source="clienteId" reference="clientes" validate={[required()]}>
                    <SelectInput optionText="nombre" />
                </ReferenceInput>

                <ReferenceInput label="Vendedor" source="vendedorId" reference="vendedores" validate={[required()]}>
                    <SelectInput optionText="nombre" />
                </ReferenceInput>

                <TextInput source="interes" label="Interés"></TextInput>
                <TextInput source="cuotas" label="Cuotas"></TextInput>*/}
                <ReferenceInput label="Venta" source="ventaId" reference="ventas" validate={[required()]}>
                    <SelectInput optionText="id" />
                </ReferenceInput>
            </SimpleForm>
        </Create>
    )
}

export const PagoEdit = (props) => {
    return (
        <Edit {...props} title="SISTEMA DE GESTION | Editar Pago" >
            <SimpleForm variant="standard">
            
                <ReferenceInput label="Cliente" source="clienteId" reference="clientes" validate={[required()]}>
                    <SelectInput optionText="nombre" />
                </ReferenceInput>
                <DateInput source="fecha" label="Fecha"/>
                <TextInput source="importe" label="Importe"/>
                <TextInput source="observaciones" label="Observaciones"/>


                {/*<DateInput source="fecha" label="Fecha"/>

                <ReferenceInput label="Cliente" source="clienteId" reference="clientes" validate={[required()]}>
                    <SelectInput optionText="nombre" />
                </ReferenceInput>

                <ReferenceInput label="Vendedor" source="vendedorId" reference="vendedores" validate={[required()]}>
                    <SelectInput optionText="nombre" />
                </ReferenceInput>
                <TextInput source="interes" label="Interés"></TextInput>
                <TextInput source="cuotas" label="Cuotas"></TextInput>
                
                <VentaEdits />*/}
                                          
            </SimpleForm>
        </Edit>
    )
}
