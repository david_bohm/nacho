import React, { useState, useEffect } from 'react';
import axios from 'axios';
import RichTextInput from 'ra-input-rich-text';
import { Button, TextField as Field } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import config from '../conf';
import { useInput } from 'react-admin';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    DateField,
    DateTimeField,
    BooleanField,
    RichTextField,
    ChoiceField,
    ReferenceField,
    Filter
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    RadioButtonGroupInput,
    Edit,
    SimpleForm,
    DateInput,
    DisabledInput,
    AutocompleteInput,
    TextInput,
    ReferenceInput,
    SelectInput,
    NumberInput,
    BooleanInput,
    ArrayInput,
    SimpleFormIterator,
    ImageInput,
    ImageField
} from 'react-admin';
import {
    Create,
    SaveButton,
    Toolbar,
    required
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

import MyImageField from '../imageField';

//import RichTextInput from 'ra-input-rich-text';

import conf from '../conf';

const Auto = props => {
    const {
        input: { name, onChange, ...rest },
        meta: { touched, error },
        isRequired
    } = useInput(props);

    return (
        <>
            <p>{props.label}</p>
            <Autocomplete
                placeholder="Hola"
                freeSolo
                onChange={(e, v) => {
                    if (v) {
                        onChange(v.value)
                    }
                }}
                onInputChange={e => onChange(e.target.value)}
                id="combo-box-demo"
                options={props.options}
                getOptionLabel={(option) => option.value}
                size="small"
                style={{ width: "400px", minWidth: "400px"}}
                renderInput={(params) => <Field {...params} variant="outlined" />}
            />
        </>
    );
};

const Direccion = props => {
    const { source, ...rest } = props;

    return (
        <span>
            <Auto source="direccion" label="Direccion" validate={required()} options={props.options} {...rest} />
            &nbsp;
        </span>
    );
};

const Documento = props => {
    const { source, ...rest } = props;

    return (
        <span>
            <Auto source="documento" label="Documento" validate={required()} {...rest} options={props.options} />
            &nbsp;
        </span>
    );
};

const onFileSelected = async e => {
    console.log('file', e)
    const formData = new FormData();

    formData.append('image', e[0]);

    try {
        const upload = await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });

        console.log('upload success', upload);

    } catch (error) {
        console.log('error', error);
    }
}

const onFilesSelected = async e => {
    console.log('files', e)

    try {
        const result = await Promise.all(
            e.map(async file => {
                const formData = new FormData;

                formData.append('file', file);

                return await axios.post(`${conf.CONTAINER_URL}/upload`, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                });
            })
        );

        console.log('FINAL', result);
    } catch (error) {
        console.log('error', error);
    }
}

//////////////////////////////////////

/*const PropiedadFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="codigo" alwaysOn />
        <ReferenceInput label="Categoria" source="CategoriaId" reference="Categorias" allowEmpty>
            <SelectInput optionText="nombre" />
        </ReferenceInput>
    </Filter>
);*/

const VerCuentaCorriente = props => {
    const navegar = () => {
        window.location.href = `/#/clientes/${props.record.id}/cuentaCorriente`;
    }

    return (
        <Button onClick={navegar}>Cuenta corriente</Button>
    )
}

export const ClienteList = (props) => {
    return (
        <List {...props} title="SISTEMA DE GESTION | Lista de Clientes" filters={<Filter {...props}>
            <TextInput label="Buscar por nombre" source="nombre" alwaysOn />
        </Filter>
        }>
            <Datagrid>

                <TextField source="nombre" label="Nombre" />
                <TextField source="apellido" label="Apellido" />
                <TextField source="documentoTipo" label="Documento" />
                <TextField source="documento" label="Número" />
                <TextField source="direccion" label="Dirección" />
                <TextField source="localidad" label="Localidad" />
                <TextField source="telefono" label="Teléfono" />
                <TextField source="categoriaCliente" label="Categoría cliente" />
                <ReferenceField label="Vendedor" source="vendedorId" reference="vendedores" allowEmpty={true}>
                    <TextField source="nombre" />
                </ReferenceField>
                <EditButton />
                <VerCuentaCorriente />

            </Datagrid>
        </List>
    )
}

export const ClienteCreate = (props) => {
    const [documentos, setDocumentos] = useState([]);
    const [direcciones, setDirecciones] = useState([]);

    useEffect(() => {
        axios.get(config.API_URL + '/Clientes', {
            params: {
                filter: {
                    fields: {
                        documento: true,
                        direccion: true
                    }
                }
            }
        })
            .then(response => {
                const _docs = [];
                const _dirs = [];

                response.data.forEach(item => {
                    _docs.push({ value: String(item.documento) });
                    _dirs.push({ value: item.direccion });
                });

                setDocumentos(_docs);
                setDirecciones(_dirs);
            });
    }, []);

    return (
        <Create {...props} title="SISTEMA DE GESTION | Crear Cliente" >
            <SimpleForm variant="standard">
                
                
                <TextInput source="nombre" label="Nombre" />
                <TextInput source="apellido" label="Apellido" />

                <RadioButtonGroupInput source="documentoTipo" choices={[
                    { id: 'dni', name: 'DNI' },
                    { id: 'lc', name: 'LC' },
                    { id: 'le', name: 'LE' },
                ]} />

                <Documento options={documentos} label="Documento"/>
                <Direccion options={direcciones} label="Direccion"/>
                <SelectInput source="localidad" choices={[
                    { id: 'rosario', name: 'Rosario' },
                    { id: 'sanLorenzo', name: 'San Lorenzo' },
                    { id: 'capitanBermudez', name: 'Capitán Bermúdez' },
                    { id: 'oliveros', name: 'Oliveros' },
                    { id: 'puertoGaboto', name: 'Puerto Gaboto' },
                    { id: 'maciel', name: 'Maciel' },
                    { id: 'monge', name: 'Monge' },
                    { id: 'barrancas', name: 'Barrancas' },
                    { id: 'arocena', name: 'Arocena' },
                    { id: 'galvez', name: 'Galvez' },
                    { id: 'sanLorenzo', name: 'San Lorenzo' },

                ]} />
                <TextInput source="telefono" label="Teléfono" />

                <RadioButtonGroupInput source="categoriaCliente" choices={[
                    { id: 'buena', name: 'Buena' },
                    { id: 'regular', name: 'Regular' },
                    { id: 'mala', name: 'Mala' },
                ]} />
                <TextInput source="observaciones" label="Observaciones" />
                <ReferenceInput label="Vendedor" source="vendedorId" reference="vendedores" allowEmpty={true}>
                <SelectInput optionText={(record) => record.nombre + " " + record.apellido } />
                </ReferenceInput>
            </SimpleForm>
        </Create>
    )
}

export const ClienteEdit = (props) => {
    console.log(props);
    return (
        <Edit {...props} title="SISTEMA DE GESTION | Editar Cliente" >
            <SimpleForm variant="standard">
                <TextInput source="nombre" label="Nombre" />
                <TextInput source="apellido" label="Apellido" />
                <RadioButtonGroupInput source="documentoTipo" choices={[
                    { id: 'dni', name: 'DNI' },
                    { id: 'lc', name: 'LC' },
                    { id: 'le', name: 'LE' },
                ]} />
                <TextInput source="documento" label="Número" />
                <TextInput source="direccion" label="Dirección" />
                <SelectInput source="localidad" choices={[
                    { id: 'rosario', name: 'Rosario' },
                    { id: 'sanLorenzo', name: 'San Lorenzo' },
                    { id: 'capitanBermudez', name: 'Capitán Bermúdez' },
                ]} />
                <TextInput source="telefono" label="Teléfono" />

                <RadioButtonGroupInput source="categoriaCliente" choices={[
                    { id: 'buena', name: 'Buena' },
                    { id: 'regular', name: 'Regular' },
                    { id: 'mala', name: 'Mala' },
                ]} />
                <TextInput source="observaciones" label="Observaciones" />

                <ReferenceInput label="Vendedor" source="vendedorId" reference="vendedores" allowEmpty={true}>
                    <SelectInput optionText={(record) => record.nombre + " " + record.apellido } />
                </ReferenceInput>
            </SimpleForm>
        </Edit>
    )
}
